The humans like to memorialize dates by holding annual celebrations.
What kinds of dates would a hacker culture memorialize?

* January 1, 1992: Grace Hopper died (born December 9, 1906)
* January 8, 1642: Galileo Galilei died after 8.5 years of house
  arrest (born February 15, 1564, sentenced by the Inquisition June
  22, 1633(?))
* January 11, 2013: Aaron Hillel Swartz committed suicide to escape
  government persecution (born November 8, 1986)
* January 14, 1901: Tarski born (died October 26, 1983)
* February 7, 1990: Alan Perlis died (born April 1, 1922)
* Feburary 8, 1920: Bob Bemer born (died June 22, 2004)
* February 11, 1897: Emil Post born (died April 21, 1954)
* February 13, 1258: the destruction of the House of Wisdom (بيت
  الحكمة‎) by Mongol soldiers in the Siege of Baghdad (though that was
  only the first day of a week of destruction)
* February 17, 1600: Giordano Bruno, who first proposed that the stars
  were distant suns, burned at the stake for, among other things,
  teaching reincarnation and possessing the writings of Erasmus.
* February 23, 1855: Gauss died (born April 30, 1777)
* March 1, 1990: the Secret Service raided Steve Jackson Games for
  publishing GURPS Cyberpunk
* March 1, 86 BCE: [Sulla sacked
  Athens](https://en.wikipedia.org/wiki/Siege_of_Athens_and_Piraeus_(87%E2%80%9386_BC)),
  having burned Plato's Academy.  (However, this needs to be corrected
  for calendar alignment.)
* March 5, 1574: William Oughtred born (died June 30, 1660)
* March 7, 1917: Betty Holberton born (died December 8, 2001)
* March 11, 1890: Vannevar Bush born (died June 28, 1974)
* March 18, 1964: Norbert Wiener died (born November 26, 1894)
* March 20, 1956: Kurt Gödel (born April 28, 1906, died January 14,
  1978) wrote to John von Neumann, posing essentially the P vs. NP
  problem
* March 21, 1768: Fourier born (or, alternatively, December 21, 1807,
  he presented his paper "on the propagation of heat in solid bodies")
* March 23, 1928: Jean E. Sammet born (died May 20, 2017)
* March 25, 1914: Norman Borlaug born, who saved a billion lives with
  dwarf wheat (died September 12, 2009)
* March 31, 1596: Descartes' birth (died February 11, 1650)
* April 7, 1761: Thomas Bayes died (birthdate unknown)
* April 15, 1707: Euler born (died September 18, 1783)
* April 25, 1903: Kolmogorov born (died October 20, 1987)
* April 29, 1911: founding of Tsinghua University (then 清華學堂, now
  清华大学)
* April 30, 1916: Claude Shannon born (died February 24, 2001)
  (Shannon Day was celebrated April 30, 2016)
* May 2, 1519: Leonardo da Vinci died (born April 14 or 15, 1452)
* May 7, 1711 (O.S. April 26): David Hume born. (Died August 25, 1776)
* May 10, 1933: nationwide book burning by Nazis, following the German
  "Law for the Restoration of the Professional Civil Service", which
  on April 7, 1933 eliminated all Jewish and Communist public
  employees, including professors, with some exceptions;
* May 11, 1918: Richard Feynman born (died February 15, 1988)
* May 17, 1902: the discovery that the Antikythera Mechanism had gears
  (probably brought up July 1901)
* May 20, 1921: Hao Wang (王浩) born (died May 13, 1995)
* May 31, 1832: Évariste Galois killed in a duel (born October 25, 1811)
* June 7, 1954: Alan Turing committed suicide (born June 23, 1912)
* June 16, 1915: John Tukey born (died July 26, 2000)
* June 22, 1910: Konrad Zuse born (died December 18, 1995)
* June 27, 1831: Sophie Germain (Monsieur Antoine-Auguste Leblanc) died
* June 28: Tau Day
* June 30, 1992: OpenGL released
* June 30, feast day of Ramon Llull, whose works were prohibited by
  the Spanish Inquisition (traditional death date June 29)
* July 1, 1646: Leibniz's birth in Leipzig (O.S. June 21) (died
  November 14, 1716)
* July 10, 1856: Nikola Tesla (Никола Тесла) born (died January 7,
  1943)
* July 16, 1945: the Trinity event
* July 17, 1912: Poincaré died (born April 29, 1854)
* July 25, 1926: Ray Solomonoff born (died December 7, 2009)
* August 6, 2002: Dijkstra died (born May 11, 1930)
* August 8, 1900: David Hilbert (born January 23, 1862; died February
  14, 1943) presents ten of his 23 famous problems at the
  International Congress of Mathematicians in Paris.
* August 9, 1927: Marvin Minsky born (died January 24, 2016)
* August 12, 2013: Warren Teitelman died (born 1941)
* September 5, 1977: launch of Voyager 1, omitting "Here Comes the
  Sun" for copyright reasons
* September 17, 1826: Riemann born
* September 26, 1905: Einstein published the theory of relativity.
  Einstein was born on March 14, 1879, and died on April 18, 1955.  In
  his *annus mirabilis* 1905, he published relativity and some other
  stuff, including his thesis (April 30).  *Annalen der Physik*
  received the relativity paper on June 30 and published it September
  26.
* September 27, 1983: The inauguration of the GNU Project
* October 18, 1931: Thomas Edison died (born February 11, 1847)
* October 29, 1998: the sale of the Archimedes Palimpsest
* the fourth month of the inundation season: when Ahmose wrote the
  Rhind Papyrus
* November 2, 1988: the helminthiasis of the internet with the Morris
  worm
* November 6, 1717: J. S. Bach imprisoned (or March 31, 1685:
  J. S. Bach born, or July 28, 1750, Bach died (Episcopal feast day))
* November 8, 1848: Gottlob Frege born (died July 26, 1925; published
  the Begriffsschrift in 1879) although he was an anti-Semite
* December 7, 1873: Cantor sends Dedekind his proof of the
  uncountability of the reals
* December 9, 1968: Doug Engelbart's Mother of All Demos (born
  January 30, 1925; died July 2, 2013)
* December 28, 1903: John von Neumann born (died February 8, 1957)

* Lu Ban?
* Zhang Heng (張衡) (dates unknown)
* Su Song (蘇頌) (dates unknown)
* Guo Shoujing (郭守敬) (dates unknown)
* Sunshu Ao (孫叔敖) (dates unknown)
* Shen Kuo (沈括) (dates unknown)
* Yī Xíng (一行) (dates unknown)
* Liu Hui (劉徽) (dates unknown)
* Mozi (墨子) (dates unknown)
* Zu Chongzhi (祖沖之) (dates unknown)
* Heron of Alexandria (dates unknown)
* Eudoxus of Cnidus (dates unknown) (when was his eclipse?)
* Ahmad, Muhammad and Hasan bin Musa ibn Shakir, the Banu Musa who
  wrote the كتاب الحيل Kitab al-Hiyal (dates unknown) in the House of
  Wisdom
* the first solar power plant entered production in Egypt
* the publication of the first Scheme paper?
* the Russian who didn't launch the nukes
* Chomsky hierarchy?
  <https://doi.org/10.1016%2FS0019-9958%2859%2990362-6> 1959 "On
  certain formal properties of grammars" (though Chomsky himself is
  still alive)
* Aristotle
* Something about Knuth?
* Stephen A. Cook's SAT paper establishing NP-completeness?
  <http://www.cs.toronto.edu/~sacook/homepage/1971.pdf>
  <https://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.406.395>
* something to memorialize the Mohists and the other scholars who fell
  to Qin Shi Huang?
* something to memorialize the Library of Alexandria?
* Johann Bernoulli?
* the SIMTEL20 shutdown
* something to celebrate Euclid
* the birth or death of Dennis Ritchie
* the founding of Wikipedia
* something about Babbage; maybe Lovelace's publication of the first
  program?
* al-Khwarizmi's book
  <https://en.wikipedia.org/wiki/The_Compendious_Book_on_Calculation_by_Completion_and_Balancing>?
* something to celebrate Aryabhata and the Aryabhatiya?
* McCarthy's or Russell's Lisp
* publication of the break of MD5
* Brahmagupta
* Newton's death or publication of Principia (July 5, 1687?) or birth (Dec 25)
* Boole's birth
* publication of the break of Merkle's knapsack algorithm
* something about the Nine Chapters on the Mathematical Art
* Laplace's birth, or the publication of his probability theory
* the release of the first version of Haskell
* St. Columba's war
* Ramanujan's death
* Apollo's landing on the moon
* Kantorovich's birth or his publication of "Математические методы
  организации и планирования производства"
* the burning of the Maya papyri
* the birth of Emmy Noether
* the publication of Alice in Wonderland
* Champollion's decipherment of the hieroglyphs
* the end of the NSFNet AUP
* the founding of Sun
* the going on sale of the Altair 8800
* founding of the University of Leipzig
* burning of the last copy of the Yongle Encyclopedia
* Cornelis Drebben
* Jaquet Droz?
* Inauguration of the EDVAC?
* Alexander Humboldt?
* the defeat of Kasparov
* something about Prometheus
* the rescue of the library of Timbuktu
* Mozart?
* launch of Sputnik
* Haskell released
* THERAC-25
* Ariane 5
* Chernobyl
* Lavoisier's execution
* Linux announced
* 4.4BSD-Lite released
* Jean Bartik?  May have made ENIAC a stored-program computer.
* Kalashnikov?

Of secondary importance:

* February 13, 1805: Dirichlet born
* October 30, 1632: (O.S. October 20) Christopher Wren born (died
  March 8, 1723 (O.S. February 25))
* Rudolf Carnap

Rejected:

* Kathleen Booth? no, she's still alive
* Michael Rabin (no, he's still alive)
* September 10, 1839: Charles Sanders Peirce born (died April 19,
  1914) (but he supported racism-based slavery, and wasn't as
  important as other significant hackers in the history of logic)
* Ed Fredkin (no, he's still alive)
* Ivan Sutherland (no, he's still alive)

